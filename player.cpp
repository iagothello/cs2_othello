#include "player.h"
#include <cstdlib>
#include <vector>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	mySide = side;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
	if (side == BLACK)
	{
		oppSide = WHITE;
	}
	else
	{
		oppSide = BLACK;
	}
	
	//Board *board = Board->copy();
	board = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
	if (opponentsMove != NULL)
	{
		//update board with opponents move
		board->doMove(opponentsMove, oppSide);
		std::cerr << "opp move: "<< opponentsMove->getX()<<" "<<
									opponentsMove->getY()<<std::endl;
	}else{
	std::cerr << "opp has no move"<<std::endl;
	}	
	
	if (board->hasMoves(mySide))
    {
		Move *move = randomMove();
		board->doMove(move, mySide);
		std::cerr << "returning my move to " << move->getX() << 
										" "<<move->getY() <<std::endl;
		return move;
	}
    
  
	
	return NULL;
}

Move *Player::randomMove() 
{
	std::vector <int> posMoves;
	//int posMoves_count = 0;
	for (int i = 0; i<8; i++)
	{
		for (int j = 0; j<8; j++)
		{
			if (board->occupied(i, j)){
				continue;
			}
			
			Move *posMove = new Move(i,j);
			
			if (board->checkMove(posMove, mySide))
			{
				posMoves.push_back(i*8 + j);
				std::cerr << i<<" "<< j<<" is pushed back"<<std::endl;
	
				//posMoves_count += 1;
				//return posMove;
			}else{
				std::cerr << i<<" "<< j<<"not poss"<<std::endl;
			}
			delete posMove;
			
		}
	}
	//Move *debugMove = new Move(3,2);
	//return debugMove;
	//int rand = rand()%posMoves_count += 1;
	
	//for (std:vector<Moves>::iterator i = posMoves.begin(); i != posMoves.end(); i++)
	std::cerr << posMoves.size()<<" poss moves"<<std::endl;
	int coord = posMoves[rand()%posMoves.size()];
	//int coord = posMoves[0];
	int x_new = coord / 8;
	int y_new = coord % 8;
	
	Move *newMove = new Move(x_new, y_new);
	std::cerr << "i move to "<< newMove->getX() <<" "<< newMove->getY()<<std::endl;
return newMove;
}
