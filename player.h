#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:				
    Player(Side side);
    ~Player();
    Board *board;
    Side oppSide;
    Side mySide;

    Move *doMove(Move *opponentsMove, int msLeft);
    Move *randomMove();
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
